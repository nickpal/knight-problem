<?php

include_once 'Knight.php';

class LLNode
{
    private $knight;
    /**
     * @var LLNode
     */
    private $next;
    /**
     * @var LLNode
     */
    private $prev;

    /**
     * @return mixed
     */
    public function getPrev()
    {
        return $this->prev;
    }

    /**
     * @param mixed $prev
     */
    public function setPrev($prev): void
    {
        $this->prev = $prev;
    }

    /**
     * @return LLNode
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param mixed $next
     */
    public function setNext($next): void
    {
        $this->next = $next;
    }

    /**
     * @return Knight
     */
    public function getKnight()
    {
        return $this->knight;
    }

    /**
     * @param mixed $knight
     */
    public function setKnight($knight): void
    {
        $this->knight = $knight;
    }
}

class LinkedList
{
    /**
     * @var int
     */
    private $length;
    /**
     * @var LLNode
     */
    private $current;
    /**
     * @var LLNode
     */
    private $head;

    /**
     * @var LLNode
     */
    private $tail;

    public function delete()
    {
        $this->length--;
        $this->current->getPrev()->setNext($this->current->getNext());
        $this->current->getNext()->setPrev($this->current->getPrev());
        $nextNode = $this->current->getNext();
        $this->current = $nextNode;
    }

    /**
     * @param LLNode $node
     * @return void
     */
    public function print($node)
    {
        echo($node->getKnight() . ' - ');
        if ($node == $this->tail || $this->length == 1)
            return;
        else {
            $this->print($node->getNext());
        }

    }

    public function multipleRounds(int $numberOfKnights = 100, int $health = 100)
    {
        for ($i = 0; $i < $numberOfKnights; $i++) {
            $k = new Knight($health);
            $this->add($k);
        }

        while ($this->length >= 1) {
            $this->oneRound();
        }
    }

    public function add(Knight $knight)
    {
        $this->length++;
        $node = new LLNode();
        $node->setKnight($knight);

        if (!$this->head) {
            $this->head = $node;
            $this->current = $this->head;
            $this->tail = $this->head;
        } else {
            $this->tail->setNext($node);
            $node->setPrev($this->tail);
            $node->setNext($this->head);
            $this->tail = &$node;
            $this->head->setPrev($node);
        }
    }

    public function oneRound()
    {
        if ($this->length == 1) {
            $this->current->getKnight()->win();
        }

        $dice = $this->current->getKnight()->throwDice();
        $nextNode = $this->current->getNext();
        $knignt = $nextNode->getKnight();
        $knignt->wound($dice);
        if ($knignt->dead()) {
            $this->deleteNode($nextNode);
        }

        $this->goNext();
    }

    /**
     * @param LLNode $node
     * @return void
     */
    public function deleteNode($node)
    {
        if ($this->head == $node)
            $this->head = $node->getNext();

        if ($this->tail == $node) {
            $this->tail = $node->getNext();
        }
        $this->length--;
        $node->getPrev()->setNext($node->getNext());
        $node->getNext()->setPrev($node->getPrev());
    }

    public function goNext()
    {
        $this->current = $this->current->getNext();
    }
}

$vars = getopt('k:h:');
$knights = isset($vars['k']) ? $vars['k'] : 1000;
$health = isset($vars['h']) ? $vars['h'] : 100;

$ll = new LinkedList();
$ll->multipleRounds($knights, $health);