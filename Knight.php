<?php

class Knight
{
    static $id = 0;

    private $name;
    private $health;

    public function __construct($health)
    {
        $this->name = 'K_' . self::$id++;
        $this->health = $health;
    }

    public function getId()
    {
        return $this->name;
    }

    public function throwDice()
    {
        return rand(1, 6);
    }

    public function wound(int $wound)
    {
        $this->health -= $wound;
    }

    public function win()
    {
        die("\n\rKnight " . $this->name . " won!\n\r");
    }

    /**
     * @return bool
     */
    public function dead()
    {
        return $this->health <= 0;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name . '=' . $this->health;
    }
}