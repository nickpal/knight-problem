<?php

include_once 'Knight.php';


class Game
{

    /**
     * @var Knight[] $knights
     */
    private $knights;
    /**
     * @var int
     */
    private $currentKnightIndex = 0;
    /**
     * @var int
     */
    private $currentDiceValue = 0;

    public function __construct(int $numberOfKnights = 100, int $health = 100)
    {
        for ($i = 0; $i < $numberOfKnights; $i++) {
            $knight = new Knight($health);
            $this->knights[] = $knight;
        }
    }

    public function play()
    {
        while (count($this->knights) >= 1) {
            $this->playOneStep();
        }
    }

    public function playOneStep()
    {
        if (count($this->knights) == 1) {
            $this->knights[0]->win();
        }

        $this->currentDiceValue = $this->getCurrentPlayer()->throwDice();
        $nextPlayer = $this->getNextPlayer();
        $nextPlayer->wound($this->currentDiceValue);
        if ($nextPlayer->dead())
            $this->removePlayer($this->getNextPlayerIndex());

        $this->currentKnightIndex = $this->getNextPlayerIndex();
    }

    public function getCurrentPlayer()
    {
        return $this->knights[$this->currentKnightIndex];
    }

    public function getNextPlayer()
    {
        return $this->knights[$this->getNextPlayerIndex()];
    }

    public function getNextPlayerIndex()
    {
        if ($this->currentKnightIndex + 1 == count($this->knights)) {
            return 0;
        } else {
            return $this->currentKnightIndex + 1;
        }
    }

    public function removePlayer($index)
    {
        array_splice($this->knights, $index, 1);
        if ($this->currentKnightIndex > $index)
            $this->currentKnightIndex--;
    }

    public function __toString()
    {
        $str = "\n\r";
        foreach ($this->knights as $key => $knight) {
            if ($key == $this->currentKnightIndex)
                $mark = '*' . $this->currentDiceValue;
            else
                $mark = '';

            $str .= $key . '--->' . ($knight) . $mark . "\n\r";
        }
        return $str;
    }
}


$vars = getopt('k:h:');
$knights = isset($vars['k']) ? $vars['k'] : 1000;
$health = isset($vars['h']) ? $vars['h'] : 100;
$g = new Game($knights, $health);
$g->play();




