# README #

There are 2 alternative solutions - sol1.php and sol2.php

### How do I get set up? ###

Just run the code in CLI, sample:
php sol1.php -k 1000 -h 100
or
php sol2.php -k 1000 -h 100

-k - number of knights
-h - health of each Knight


### Contribution guidelines ###

Sol1 - is a array based aproach to solve the problem
Sol2 - a linked list aproach

Both are pretty close in performance
